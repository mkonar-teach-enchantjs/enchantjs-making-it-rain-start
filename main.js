/** One drop **/

window.onload = myGame;
enchant();

function myGame() {
    // New game with a 320x320 pixel canavs, 24 frames/sec.
    var game = new Core(320, 320);
    game.fps = 24;

    // Preload assets.
    game.preload('droplet24.png');

    // Specify what should happen when the game loads.
    game.onload = function () {

        // Gameplay parameters
        var gravity = 6;    // Number of pixels per frame to move things down.
        
        // Create a new Sprite and associate an image with it.
        var droplet = new Sprite(24, 24);
        droplet.image = game.assets['droplet24.png'];
        
        // Put droplet in random x location and just above screen.
        droplet.x = randomInt(0, game.width - droplet.width);
        droplet.y = -1 * droplet.height; // droplet is just above top of canvas.
        
        game.rootScene.backgroundColor = 'black';
        game.rootScene.addChild(droplet);
    
        // Add an event listener to the droplet Sprite to move it.
        droplet.addEventListener(Event.ENTER_FRAME, function () {
            droplet.y += gravity;   // Move the droplet down.
        });
    };
    
    // Start the game.
    game.start();
}

// === Helper functions === //
/** Generate a random integer between low and high (inclusive). */
function randomInt(low, high) {
    return low + Math.floor((high + 1 - low) * Math.random());
}
